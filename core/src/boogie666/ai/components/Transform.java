package boogie666.ai.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Vector2;

public class Transform implements Component {
    public Vector2 position;
    public float angle;
}
