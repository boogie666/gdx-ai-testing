package boogie666.ai;

import boogie666.ai.components.AI;
import boogie666.ai.components.Physics;
import boogie666.ai.components.Transform;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.ai.steer.behaviors.Arrive;
import com.badlogic.gdx.ai.steer.behaviors.Flee;
import com.badlogic.gdx.ai.steer.behaviors.Wander;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.World;

public class EntityManager {

    private final World world;

    public EntityManager(World world) {
        this.world = world;
    }


    public Entity createBall(float x, float y){
        BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.DynamicBody;
        def.position.set(x, y);


        Body body = world.createBody(def);

        CircleShape shape = new CircleShape();
        shape.setRadius(0.5f);

        body.createFixture(shape, 1.0f);

        shape.dispose();

        final Entity ball = new Entity();

        final Physics ph = new Physics();
        ph.body = body;

        final Transform tf = new Transform();
        tf.position = body.getPosition().cpy();
        tf.angle = body.getAngle() * MathUtils.radiansToDegrees;

        final AI ai = new AI(body, true, 0.5f);
        ai.setMaxLinearAcceleration(10);
        ai.setMaxLinearSpeed(3);
        ai.setMaxAngularAcceleration(.5f);
        ai.setMaxAngularSpeed(5);

        ai.setSteeringBehavior(new Wander<>(ai)
                .setFaceEnabled(true)
                .setAlignTolerance(0.001f)
                .setDecelerationRadius(1)
                .setTimeToTarget(1f)
                .setWanderOffset(10)
                .setWanderRadius(20)
                .setWanderRate(MathUtils.PI2 * 4));

        ball.add(ph);
        ball.add(tf);
        ball.add(ai);

        return ball;

    }
}
