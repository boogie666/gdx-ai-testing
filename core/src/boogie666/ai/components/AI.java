package boogie666.ai.components;

import boogie666.ai.Utils;
import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.ai.steer.Steerable;
import com.badlogic.gdx.ai.steer.SteeringBehavior;
import com.badlogic.gdx.ai.utils.Location;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class AI implements Component, Steerable<Vector2> {
    private Body body;

    private float boundingRadius;
    private boolean tagged;

    private float maxLinearSpeed;
    private float maxLinearAcceleration;
    private float maxAngularSpeed;
    private float maxAngularAcceleration;

    private boolean independentFacing;

    private SteeringBehavior<Vector2> steeringBehavior;

    public AI (Body body, boolean independentFacing, float boundingRadius) {
        this.body = body;
        this.independentFacing = independentFacing;
        this.boundingRadius = boundingRadius;
        this.tagged = false;

        body.setUserData(this);
    }

    public boolean isIndependentFacing () {
        return independentFacing;
    }

    @Override
    public Vector2 getPosition () {
        return body.getPosition();
    }

    @Override
    public float getOrientation () {
        return body.getAngle();
    }

    @Override
    public void setOrientation (float orientation) {
        body.setTransform(getPosition(), orientation);
    }

    @Override
    public Vector2 getLinearVelocity () {
        return body.getLinearVelocity();
    }

    @Override
    public float getAngularVelocity () {
        return body.getAngularVelocity();
    }

    @Override
    public float getBoundingRadius () {
        return boundingRadius;
    }

    @Override
    public boolean isTagged () {
        return tagged;
    }

    @Override
    public void setTagged (boolean tagged) {
        this.tagged = tagged;
    }

    @Override
    public Location<Vector2> newLocation () {
        return new Loc();
    }

    @Override
    public float vectorToAngle (Vector2 vector) {
        return Utils.vectorToAngle(vector);
    }

    @Override
    public Vector2 angleToVector (Vector2 outVector, float angle) {
        return Utils.angleToVector(outVector, angle);
    }

    public SteeringBehavior<Vector2> getSteeringBehavior () {
        return steeringBehavior;
    }

    public void setSteeringBehavior (SteeringBehavior<Vector2> steeringBehavior) {
        this.steeringBehavior = steeringBehavior;
    }



    //
    // Limiter implementation
    //

    @Override
    public float getMaxLinearSpeed () {
        return maxLinearSpeed;
    }

    @Override
    public void setMaxLinearSpeed (float maxLinearSpeed) {
        this.maxLinearSpeed = maxLinearSpeed;
    }

    @Override
    public float getMaxLinearAcceleration () {
        return maxLinearAcceleration;
    }

    @Override
    public void setMaxLinearAcceleration (float maxLinearAcceleration) {
        this.maxLinearAcceleration = maxLinearAcceleration;
    }

    @Override
    public float getMaxAngularSpeed () {
        return maxAngularSpeed;
    }

    @Override
    public void setMaxAngularSpeed (float maxAngularSpeed) {
        this.maxAngularSpeed = maxAngularSpeed;
    }

    @Override
    public float getMaxAngularAcceleration () {
        return maxAngularAcceleration;
    }

    @Override
    public void setMaxAngularAcceleration (float maxAngularAcceleration) {
        this.maxAngularAcceleration = maxAngularAcceleration;
    }

    @Override
    public float getZeroLinearSpeedThreshold () {
        return 0.001f;
    }

    @Override
    public void setZeroLinearSpeedThreshold (float value) {
        throw new UnsupportedOperationException();
    }

    private static class Loc implements Location<Vector2> {

        Vector2 position;
        float orientation;

        Loc() {
            this.position = new Vector2();
            this.orientation = 0;
        }

        @Override
        public Vector2 getPosition () {
            return position;
        }

        @Override
        public float getOrientation () {
            return orientation;
        }

        @Override
        public void setOrientation (float orientation) {
            this.orientation = orientation;
        }

        @Override
        public Location<Vector2> newLocation () {
            return new Loc();
        }

        @Override
        public float vectorToAngle (Vector2 vector) {
            return Utils.vectorToAngle(vector);
        }

        @Override
        public Vector2 angleToVector (Vector2 outVector, float angle) {
            return Utils.angleToVector(outVector, angle);
        }

    }
}
