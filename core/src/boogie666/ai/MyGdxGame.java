package boogie666.ai;

import boogie666.ai.systems.AISystem;
import boogie666.ai.systems.PhysicsSystem;
import boogie666.ai.systems.RenderSystem;
import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class MyGdxGame extends ApplicationAdapter {
	private World world;
	private Camera camera;
	private Engine engine;
	private EntityManager manager;

	@Override
	public void create () {
		world = new World(Vector2.Zero, true);
		manager = new EntityManager(world);

		float viewWidth = Gdx.graphics.getWidth() / 32f;
		float viewHeight = Gdx.graphics.getHeight() / 32f;

		camera = new OrthographicCamera(viewWidth, viewHeight);
		camera.position.set(0f,0f,0f);
		camera.update();
		engine = new Engine();

		engine.addSystem(new AISystem());
		engine.addSystem(new PhysicsSystem(world));
		engine.addSystem(new RenderSystem(world, camera));


		engine.addEntity(manager.createBall(0f,0f));

	}

	@Override
	public void render () {
		engine.update(Gdx.graphics.getDeltaTime());
	}

}
