package boogie666.ai.systems;

import boogie666.ai.components.AI;
import boogie666.ai.components.Physics;
import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.ai.GdxAI;
import com.badlogic.gdx.ai.steer.SteeringAcceleration;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class AISystem extends IteratingSystem {

    private final ComponentMapper<AI> am = ComponentMapper.getFor(AI.class);
    private final ComponentMapper<Physics> pm = ComponentMapper.getFor(Physics.class);
    private final SteeringAcceleration<Vector2> steeringOutput;

    public AISystem() {
        super(Family.all(AI.class, Physics.class).get());
        this.steeringOutput = new SteeringAcceleration<>(new Vector2());
    }

    @Override
    public void update(float deltaTime) {
        GdxAI.getTimepiece().update(deltaTime);
        super.update(deltaTime);
    }

    @Override
    protected void processEntity(Entity entity, float deltaTime) {
        AI ai = am.get(entity);
        Physics p = pm.get(entity);

        if (ai.getSteeringBehavior() != null){
            ai.getSteeringBehavior().calculateSteering(steeringOutput);
        }

        applySteering(p.body, ai, deltaTime);
    }


    private void applySteering (Body body, AI ai, float deltaTime) {
        boolean anyAccelerations = false;

        // Update position and linear velocity.
        if (!steeringOutput.linear.isZero()) {
            // this method internally scales the force by deltaTime
            body.applyForceToCenter(steeringOutput.linear, true);
            anyAccelerations = true;
        }

        // Update orientation and angular velocity
        if (ai.isIndependentFacing()) {
            if (steeringOutput.angular != 0) {
                // this method internally scales the torque by deltaTime
                body.applyTorque(steeringOutput.angular, true);
                anyAccelerations = true;
            }
        } else {
            // If we haven't got any velocity, then we can do nothing.
            Vector2 linVel = ai.getLinearVelocity();
            if (!linVel.isZero(ai.getZeroLinearSpeedThreshold())) {
                float newOrientation = ai.vectorToAngle(linVel);
                body.setAngularVelocity((newOrientation - ai.getAngularVelocity()) * deltaTime); // this is superfluous if independentFacing is always true
                body.setTransform(body.getPosition(), newOrientation);
            }
        }

        if (anyAccelerations) {
            // body.activate();

            // TODO:
            // Looks like truncating speeds here after applying forces doesn't work as expected.
            // We should likely cap speeds form inside an InternalTickCallback, see
            // http://www.bulletphysics.org/mediawiki-1.5.8/index.php/Simulation_Tick_Callbacks

            // Cap the linear speed
            Vector2 velocity = body.getLinearVelocity();
            float currentSpeedSquare = velocity.len2();
            float maxLinearSpeed = ai.getMaxLinearSpeed();
            if (currentSpeedSquare > maxLinearSpeed * maxLinearSpeed) {
                body.setLinearVelocity(velocity.scl(maxLinearSpeed / (float)Math.sqrt(currentSpeedSquare)));
            }

            // Cap the angular speed
            float maxAngVelocity = ai.getMaxAngularSpeed();
            if (body.getAngularVelocity() > maxAngVelocity) {
                body.setAngularVelocity(maxAngVelocity);
            }
        }
    }

}
